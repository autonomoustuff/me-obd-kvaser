#include "vehicle_data.h"

using namespace std;
using namespace AS::MEOBDKvaser;

BrakeData::BrakeData()
{
    _braking = false;
}

BrakeData::~BrakeData()
{
}

bool BrakeData::get()
{
    lock_guard<mutex> lock(_mu);
    return _braking;
}

void BrakeData::set(bool newBrakeStatus)
{
    lock_guard<mutex> lock(_mu);
    _braking = newBrakeStatus;
}

SpeedData::SpeedData()
{
    _speed = 0;
}

SpeedData::~SpeedData()
{
}

short SpeedData::get()
{
    lock_guard<mutex> lock(_mu);
    return _speed;
}

void SpeedData::set(short newSpeed)
{
    lock_guard<mutex> lock(_mu);
    _speed = newSpeed;
}
