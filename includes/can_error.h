#ifndef CAN_ERROR_H
#define CAN_ERROR_H

#include <cstdio>
#include <mutex>
#include <canlib.h>

namespace AS
{
    namespace MEOBDKvaser
    {
        class CanError
        {
            public:
                CanError();
                ~CanError();
                void print(CanHandle h, const char * msgText);
            private:
                std::mutex _mu;
        };
    }
}

#endif
