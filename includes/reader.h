#ifndef READER_H
#define READER_H

#include "common.h"
#include <thread>
#include "thread_status.h"

namespace AS
{
    namespace MEOBDKvaser
    {
        class Reader
        {
            public:
                Reader(bool log);
                ~Reader();
                void start();
                void join();
                void stop();
            private:
                void read_messages();
                const static char channel = 0;
                const static unsigned long msgTimeout = 30;
                const static long acceptCode = 0xFFFL;
                const static long acceptMask = 0xFFFL;
                bool logging;
                AS::MEOBDKvaser::CanError ce;
                long freq;
                unsigned int tSeg1, tSeg2, sjw, noSamp, syncMode;
                std::unique_ptr<std::thread> mThread;
                ThreadStatus ts;
                CanHandle ch;
                canStatus stat;
                long idBuf;
                unsigned char msgBuf[8];
                unsigned int dlcBuf, flagBuf;
                unsigned long timeBuf;
        };
    }
}

#endif
