#ifndef THREAD_STATUS_H
#define THREAD_STATUS_H

#include "common.h"

namespace AS
{
    namespace MEOBDKvaser
    {
        class ThreadStatus
        {
            public:
                ThreadStatus();
                ~ThreadStatus();
                bool is_running();
                void set_status(bool newStatus);
            private:
                bool _running;
                std::mutex _mu;
        };
    }
}

#endif
