#ifndef WRITER_H
#define WRITER_H

#include "common.h"
#include <thread>
#include "thread_status.h"

namespace AS
{
    namespace MEOBDKvaser
    {
        class Writer
        {
            public:
                Writer(bool log);
                ~Writer();
                void start();
                void stop();
                void join();
            private:
                void write_messages();
                const static long freq = canBITRATE_250K;
                const static char channel = 1;
                bool logging;
                AS::MEOBDKvaser::CanError ce;
                unsigned int tSeg1, tSeg2, sjw, noSamp, syncMode;
                std::unique_ptr<std::thread> mThread;
                ThreadStatus ts;
                CanHandle ch;
                canStatus stat;
                unsigned char brakeMsg[8];
                unsigned char spdMsg[8];
        };
    }
}

#endif
