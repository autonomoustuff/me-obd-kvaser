#include "reader.h"

using namespace std;
using namespace AS::MEOBDKvaser;

Reader::Reader(bool log) :
    logging(log),
    ch(1)
{
    freq = canBITRATE_500K;
    canTranslateBaud(&freq, &tSeg1, &tSeg2, &sjw, &noSamp, &syncMode);
}

Reader::~Reader()
{
    //Clean everything up.
    canBusOff(ch);
    printf("Closing connection to channel %d.", channel);
}

void Reader::start()
{
    //if (logging) printf("Connecting to channel %d.", channel);
    cout << "Starting reader..." << endl;

    unsigned char disableEcho = 1;

    //Set up and connect to the CAN channel.
    ch = canOpenChannel(channel, canWANT_EXTENDED | canOPEN_ACCEPT_VIRTUAL);
    if (ch < 0)
    {
        ce.print(ch, "canOpenChannel failed (%s).\n");
        stop();
    }

    canSetBusParams(ch, freq, tSeg1, tSeg2, sjw, noSamp, syncMode);
    if (ch < 0)
    {
        ce.print(ch, "canSetBusParams failed (%s).\n");
        stop();
    }
    canSetBusOutputControl(ch, canDRIVER_NORMAL);
    if (ch < 0)
    {
        ce.print(ch, "canSetBusOutputControl failed (%s).\n");
        stop();
    }
    /*canIoCtl(ch, canIOCTL_SET_LOCAL_TXECHO, &disableEcho, 1);
    if (ch < 0)
    {
        ce.print(ch, "canIoCtl failed (%s).\n");
        stop();
    }*/
    canBusOn(ch);
    if (ch < 0)
    {
        ce.print(ch, "canOnBus failed (%s).\n");
        stop();
    }

    //Set acceptance filters.
    //This filters for the speed ID.
    /*stat = canAccept(ch, acceptCode, canFILTER_SET_CODE_STD);
    if (ch < 0) ce.print(ch, "canAccept for code failed (%s).\n");
    stat = canAccept(ch, acceptMask, canFILTER_SET_MASK_STD);
    if (ch < 0) ce.print(ch, "canAccept for mask failed (%s).\n");*/

    //Create the thread and start sending messages.
    mThread = unique_ptr<thread>(new thread(&Reader::read_messages, this));
}

void Reader::join()
{
    //Re-join the thread.
    mThread->join();
}

void Reader::stop()
{
    if (logging) cout << "Stopping thread..." << endl;
    ts.set_status(false);
    if (logging) cout << "Thread stopped." << endl;
}

void Reader::read_messages()
{
    bool keepGoing = true;
    int j;

    if (logging) cout << "Waiting for receipt of message." << endl;

    do
    {
        stat = canReadWait(ch, &idBuf, &msgBuf, &dlcBuf, &flagBuf, &timeBuf, msgTimeout);
        
        switch (stat)
        {
            case 0:
                //We got a message!
                /*if (logging)
                {
                    cout << hex << "Message received: ";
                    printf("ID: 0x%li - Payload: ", idBuf, static_cast<unsigned>(msgBuf[0]));

                    for (j = 0; j < dlcBuf; j++)
                    {
                        printf("%2.2x ", msgBuf[j]);
                    }

                    printf("\n");
                }*/

                if (idBuf == 0x11c)
                {
                    //byte 4/5
                    //KPH
                    //0.0078125
                    //Set the speed.
                    short speed = msgBuf[4];
                    speed <<= 8;
                    speed |= msgBuf[5];
                    VehicleData::speed.set(static_cast<short>(speed * 0.0078125));
                }

                if (idBuf == 0x140)
                {
                    //Len: 2
                    //Byte 0: bit 2/3
                    //1 = braking
                    //Set the braking status.
                    unsigned char brakeStatus = (msgBuf[0] >> 2) & 0x03;

                    if (brakeStatus == 1)
                    {
                        VehicleData::braking.set(true);
                    }
                    else
                    {
                        VehicleData::braking.set(false);
                    }
                }

                break;
            case canERR_NOMSG:
                //We just didn't receive anything.
                break;
            default:
                //ERROR
                ce.print(stat, "Error receiving message (%s).\n");
                break;
        }

        keepGoing = ts.is_running();
    }
    while (keepGoing);
}
