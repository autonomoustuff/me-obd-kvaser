#include "writer.h"

using namespace std;
using namespace AS::MEOBDKvaser;

Writer::Writer(bool log) :
    logging(log),
    ch(1)
{
}

Writer::~Writer()
{
    //Clean everything up.
    canWriteSync(ch, 500);
    canBusOff(ch);
    printf("Closing connection to channel %d.", channel);
}

void Writer::start()
{
    //if (logging) printf("Connecting to channel %d.", channel);
    cout << "Starting writer..." << endl;

    //Create the thread and start sending messages.
    mThread = unique_ptr<thread>(new thread(&Writer::write_messages, this));
}

void Writer::join()
{
    //Re-join the thread.
    mThread->join();
}

void Writer::stop()
{
    if (logging) cout << "Stopping thread..." << endl;
    ts.set_status(false);
    if (logging) cout << "Thread stopped." << endl;
}

void Writer::write_messages()
{
    bool keepGoing = true;
    bool braking = false;
    short speed = 0;
    unsigned char disableEcho = 1;

    //Set up and connect to the CAN channel.
    ch = canOpenChannel(channel, canWANT_EXTENDED | canOPEN_ACCEPT_VIRTUAL);
    if (ch < 0)
    {
        ce.print(ch, "canOpenChannel failed (%s).\n");
        stop();
    }

    canSetBusParams(ch, freq, tSeg1, tSeg2, sjw, noSamp, syncMode);
    if (ch < 0)
    {
        ce.print(ch, "canSetBusParams failed (%s).\n");
        stop();
    }

    canSetBusOutputControl(ch, canDRIVER_NORMAL);
    if (ch < 0)
    {
        ce.print(ch, "canSetBusOutputContro failed (%s).\n");
        stop();
    }

    /*canIoCtl(ch, canIOCTL_SET_LOCAL_TXECHO, &disableEcho, 1);
    if (ch < 0)
    {
        ce.print(ch, "canIoCtl failed (%s).\n");
        stop();
    }*/

    canBusOn(ch);
    if (ch < 0)
    {
        ce.print(ch, "canOnBus failed (%s).\n");
        stop();
    }

    if (logging) cout << "Sending message..." << endl;

    //Begin sending the message every 20ms.
    do
    {
        canBusOff(ch);

        //Prepare the message data.
        memset(brakeMsg, 0, sizeof(brakeMsg));
        memset(spdMsg, 0, sizeof(spdMsg));

        float curSpd = VehicleData::speed.get();
        if (logging) cout << "KPH: " << curSpd << endl;
        //Get the current vehicle data.
        braking = VehicleData::braking.get();
        speed = (short)((curSpd * 0.62137) / 0.0014);
        //speed = 4000;

        if (logging) cout << "Speed: " << speed << endl;
        //Set the braking value.
        if (braking) brakeMsg[4] = 0x01;
        //msg[0] = 0x20;

        //Set the speed value.
        spdMsg[0] = ((speed >> 8) & 0xFF);
        spdMsg[1] = speed & 0xFF;

        /*msg[3] = 0xFF;
        msg[4] = 0xFF;
        msg[5] = 0xFF;
        msg[6] = 0xFF;
        msg[7] = 0xFF;*/

        if (logging)
        {
            cout << hex << "Writing message: ";
            printf("ID: 0x17c - Payload: 0x%u, 0x%u, 0x%u, 0x%u, 0x%u, 0x%u, 0x%u, 0x%u.\n", static_cast<unsigned>(brakeMsg[0]), static_cast<unsigned>(brakeMsg[1]), static_cast<unsigned>(brakeMsg[2]), static_cast<unsigned>(brakeMsg[3]), static_cast<unsigned>(brakeMsg[4]), static_cast<unsigned>(brakeMsg[5]), static_cast<unsigned>(brakeMsg[6]), static_cast<unsigned>(brakeMsg[7]));
            printf("ID: 0x158 - Payload: 0x%u, 0x%u, 0x%u, 0x%u, 0x%u, 0x%u, 0x%u, 0x%u.\n", static_cast<unsigned>(spdMsg[0]), static_cast<unsigned>(spdMsg[1]), static_cast<unsigned>(spdMsg[2]), static_cast<unsigned>(spdMsg[3]), static_cast<unsigned>(spdMsg[4]), static_cast<unsigned>(spdMsg[5]), static_cast<unsigned>(spdMsg[6]), static_cast<unsigned>(spdMsg[7]));
        }

        canBusOn(ch);
        if (ch < 0)
        {
            ce.print(ch, "canOnBus failed (%s).\n");
            stop();
        }

        //Write message.
        stat = canWriteWait(ch, 0x17c, brakeMsg, 8, canMSG_STD, 100);
        if (stat < 0) ce.print(stat, "canWrite failed (%s).\n");

        stat = canWriteWait(ch, 0x158, spdMsg, 8, canMSG_STD, 100);
        if (stat < 0) ce.print(stat, "canWrite failed (%s).\n");

        //Sleep now.
        this_thread::sleep_for(chrono::milliseconds(30));

        keepGoing = ts.is_running();
    }
    while (keepGoing);
}
