#include "me_obd_kvaser.h"

using namespace std;
using namespace AS::MEOBDKvaser;

BrakeData VehicleData::braking;
SpeedData VehicleData::speed;
unique_ptr<Writer> wr;
unique_ptr<Reader> re;
        
void my_handler(int s)
{
    printf("Caught signal %d. Shutting down OBD-II producer...\n", s);
    
    //Stop both threads from producing messages.
    wr->stop();
    re->stop();
    exit(0);
}

int main(int argc, char* argv[])
{
    struct sigaction sigIntHandler;

    sigIntHandler.sa_handler = my_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;

    sigaction(SIGINT, &sigIntHandler, NULL);

    int c;
    bool logging = false;
    bool printHelp = false;

    while ((c = getopt(argc, argv, "oh")) != -1)
    {
        switch (c)
        {
            case 'o':
                logging = true;
                break;
            case 'h':
                printHelp = true;
                break;
            default:
                abort();
        }
    }

    //Do stuff here.
    if (printHelp)
    {
        //Print the help menu and exit.
        exit(0);
    }
    else
    {
        cout << "Starting OBD-II producer for MobilEye." << endl;

        wr = unique_ptr<Writer>(new Writer(logging));
        re = unique_ptr<Reader>(new Reader(logging));

        //Start both threads.
        wr->start();
        re->start();

        //Wait for both threads to finish before exiting.
        wr->join();
        re->join();
    }

    return 0;
}
