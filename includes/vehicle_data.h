#ifndef VEHICLE_DATA_H
#define VEHICLE_DATA_H

#include <mutex>

namespace AS
{
    namespace MEOBDKvaser
    {
        class BrakeData
        {
            public:
                BrakeData();
                ~BrakeData();
                bool get();
                void set(bool newBrakeStatus);
            private:
                std::mutex _mu;
                bool _braking;
        };

        class SpeedData
        {
            public:
                SpeedData();
                ~SpeedData();
                short get();
                void set(short newSpeed);
            private:
                std::mutex _mu;
                short _speed;
        };

        class VehicleData
        {
            public:
                static BrakeData braking;
                static SpeedData speed;
        };
    }
}

#endif
