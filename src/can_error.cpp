#include "can_error.h"

using namespace std;
using namespace AS::MEOBDKvaser;

CanError::CanError()
{
}

CanError::~CanError()
{
}

void CanError::print(CanHandle h, const char * msgText)
{
    lock_guard<mutex> lock(_mu);
    char msg[64];
    canGetErrorText((canStatus)h, msg, sizeof(msg));
    fprintf(stderr, msgText, msg);
}
