#include "thread_status.h"

using namespace std;
using namespace AS::MEOBDKvaser;

ThreadStatus::ThreadStatus()
{
    _running = true;
}

ThreadStatus::~ThreadStatus()
{
}

bool ThreadStatus::is_running()
{
    lock_guard<mutex> lock(_mu);
    return _running;
}

void ThreadStatus::set_status(bool newStatus)
{
    lock_guard<mutex> lock(_mu);
    _running = newStatus;
}
